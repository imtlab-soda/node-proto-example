The example js proto library rules are taken from https://github.com/rules-proto-grpc/rules_proto_grpc/tree/master/example/js/js_proto_library. 
Protos that depend upon other protos produce JS libraries that have require statements that don't resolve at runtime. 

The failed path resolution can be seen by running

`bazel run //:test_protos`
